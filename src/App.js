import React from 'react';
import { Switch, Route, Redirect, NavLink } from 'react-router-dom';

import Topics from './components/topics/Topics';
import Archives from './components/archives/Archives';
import Pages from './components/pages/Pages';

import './App.scss';

const App = () => (
  <div className="app">
    <div className="app__header">
      <h1>Browse Site</h1>
      <h2>Select a Tab</h2>
    </div>
    <div className="app__body">
      <nav className="app__menu">
        <ul>
          <li className="app__menuItem">
            <NavLink to="/topics" activeClassName="active">Topics</NavLink>
          </li>
          <li className="app__menuItem">
            <NavLink to="/archives" activeClassName="active">Archives</NavLink>
          </li>
          <li className="app__menuItem">
            <NavLink to="/pages" activeClassName="active">Pages</NavLink>
          </li>
        </ul>
      </nav>
      <div className="app__content">
        <Switch>
          <Route exact path="/topics" component={Topics} />
          <Route exact path="/archives" component={Archives} />
          <Route exact path="/pages" component={Pages} />
          {/* Base route randomly selects one of the three available ones and redirects to it. */}
          <Redirect
            exact
            from="/"
            to={`/${['topics', 'archives', 'pages'][Math.round(Math.random() * ((2 - 0) + 0))]}`}
          />
        </Switch>
      </div>
    </div>
  </div>
);

export default App;
