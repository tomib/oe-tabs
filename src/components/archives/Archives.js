import React from 'react';

import ImageIcon from '../../assets/picture.svg';

import './Archives.scss';

const Archives = () => (
  <div className="archives">
    <div className="archives__item">
      <div className="archives__itemImage">
        <img src={ImageIcon} alt="Placeholder" />
      </div>
      <div className="archives__itemText">
        <span>Placeholder Text</span>
      </div>
    </div>
    <div className="archives__item">
      <div className="archives__itemImage">
        <img src={ImageIcon} alt="Placeholder" />
      </div>
      <div className="archives__itemText">
        <span>Placeholder Text</span>
      </div>
    </div>
    <div className="archives__item">
      <div className="archives__itemImage">
        <img src={ImageIcon} alt="Placeholder" />
      </div>
      <div className="archives__itemText">
        <span>Placeholder Text</span>
      </div>
    </div>
    <div className="archives__item">
      <div className="archives__itemImage">
        <img src={ImageIcon} alt="Placeholder" />
      </div>
      <div className="archives__itemText">
        <span>Placeholder Text</span>
      </div>
    </div>
    <div className="archives__item">
      <div className="archives__itemImage">
        <img src={ImageIcon} alt="Placeholder" />
      </div>
      <div className="archives__itemText">
        <span>Placeholder Text</span>
      </div>
    </div>
  </div>
);

export default Archives;
