import React, { Component } from 'react';
import { TweenMax, Circ } from 'gsap/TweenMax';

import './Topics.scss';

class Topics extends Component {
  state = {
    posts: [0, 0, 0, 0, 0, 0],
  }

  componentDidMount() {
    this.mounted = true;
    this.countPosts();
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  mounted = false;

  /* Starts a counter for each item with posts in the array. */
  countPosts = () => {
    this.state.posts.forEach((item, id) => {
      this.countUp(id);
    });
  }

  /* Counts the number of posts up using TweenMax. */
  countUp = (id) => {
    const counter = { value: 0 };
    const randomTarget = Math.round(Math.random() * 100);

    TweenMax.to(counter, 2, {
      value: randomTarget,
      onUpdate: () => {
        if (this.mounted) {
          this.setState(prevState => ({
            posts: prevState.posts.map((item, index) => (index === id ? Math.round(counter.value) : item)),
          }));
        }
      },
      ease: Circ.easeOut,
    });
  }

  render() {
    return (
      <div className="topics">
        <div className="topics__item">
          <h3>HTML Techniques</h3>
          <span>{this.state.posts[0]} posts</span>
        </div>
        <div className="topics__item">
          <h3>CSS Styling</h3>
          <span>{this.state.posts[1]} posts</span>
        </div>
        <div className="topics__item">
          <h3>Flash Tutorials</h3>
          <span>{this.state.posts[2]} posts</span>
        </div>
        <div className="topics__item">
          <h3>Web Miscellaneous</h3>
          <span>{this.state.posts[3]} posts</span>
        </div>
        <div className="topics__item">
          <h3>Site News</h3>
          <span>{this.state.posts[4]} posts</span>
        </div>
        <div className="topics__item">
          <h3>Web Development</h3>
          <span>{this.state.posts[5]} posts</span>
        </div>
      </div>
    );
  }
}

export default Topics;
