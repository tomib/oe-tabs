import React from 'react';

import './Pages.scss';

const Pages = () => (
  <div className="pages">
    <div className="pages__item">
      <h3>Home</h3>
    </div>
    <div className="pages__item">
      <h3>News</h3>
    </div>
    <div className="pages__item">
      <h3>Contact</h3>
    </div>
    <div className="pages__item">
      <h3>Careers</h3>
    </div>
  </div>
);

export default Pages;
