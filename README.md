# Oetker - Page Tabs

Page tabs with random select on root route and counter animations for posts.

Demo is available on: http://www.tomislavbisof.com/oe-tabs/

### Installation

To install all the required dependencies, run:

```
npm install
```

### Usage

For development with the Webpack Dev Server and HMR, use:

```
npm run watch
```

To build the project for production, use:

```
npm run build
```

Your project files will be located in the `/dist/` folder after building. Images smaller than 8KB will be embedded, while larger ones will be located in `/dist/assets/`.